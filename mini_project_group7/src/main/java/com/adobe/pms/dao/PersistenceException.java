package com.adobe.pms.dao;


/*
* 
* @author
* VIkas Patidar, Saurabh Gupta, Sahil Teotia
* 
*/
public class PersistenceException extends Exception {

	private static final long serialVersionUID = -8585222934351806120L;

	public PersistenceException() {
	}

	public PersistenceException(String message) {
		super(message);
	}

	public PersistenceException(Throwable cause) {
		super(cause);
	}

	public PersistenceException(String message, Throwable cause) {
		super(message, cause);
	}

	public PersistenceException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}

