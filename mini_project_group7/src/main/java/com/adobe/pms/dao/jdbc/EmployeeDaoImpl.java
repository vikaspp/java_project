package com.adobe.pms.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adobe.pms.dao.EmployeeDao;
import com.adobe.pms.dao.FetchException;
import com.adobe.pms.dao.PersistenceException;
import com.adobe.pms.entity.Employee;


/*
* 
* @author
* VIkas Patidar, Saurabh Gupta, Sahil Teotia
* 
*/
public class EmployeeDaoImpl implements EmployeeDao{

	public void addEmployee(Employee emp) throws PersistenceException, FetchException {
		Connection con=null;
		Statement stmt=null;
		int currentId = 0;
		String SQL= "SELECT max(empId) FROM Employees";
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				currentId = rs.getInt(1) +1;
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get employee",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		
		
		PreparedStatement ps=null;
		SQL="INSERT INTO Employees (empId,empName,empEmail, empDesignation) VALUES(?,?,?,?)";
		try {
			con=DBUtil.getConnection();
			ps=con.prepareStatement(SQL);
			ps.setInt(1, currentId);
			ps.setString(2, emp.getEmpName());
			ps.setString(3, emp.getEmpEmail());
			ps.setString(4,emp.getDesignation());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new PersistenceException("Unable to add Employee",e);
			
		}
		finally{
			DBUtil.releaseStatement(ps);
			DBUtil.releaseConnection(con);
		}
	}
	
@Override
	public List<Employee> getAllEmployees() throws FetchException {
		List<Employee> employees= new ArrayList<Employee>();
		Connection con=null;
		Statement stmt=null;
		String SQL= "SELECT empId, empName, empEmail, empDesignation FROM employees";
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				Employee emp = new Employee(rs.getInt("empId"),rs.getString("empName"),rs.getString("empEmail"),rs.getString("empDesignation"));
				employees.add(emp);
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get Employees",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		return employees;
	}
	
		public Employee getEmployee(int id) throws FetchException {
		Connection con=null;
		Statement stmt=null;
		String SQL= "SELECT empId, empName, empEmail, empDesignation FROM employees where empId="+id;
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				//if(rs.getInt("empId") != -1)
				{
					Employee emp=new Employee(rs.getInt("empId"),rs.getString("empName"),rs.getString("empEmail"),rs.getString("empDesignation"));
					
					
					emp.setEmpId(rs.getInt("empId"));
					emp.setEmpName(rs.getString("empName"));
					emp.setEmpEmail(rs.getString("empEmail"));
					emp.setDesignation(rs.getString("empDesignation"));
					return emp;
				}
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get Employees",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		return null;

	}


	

		

}



