package com.adobe.pms.dao.jdbc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.adobe.pms.dao.FetchException;
import com.adobe.pms.dao.PersistenceException;
import com.adobe.pms.dao.ProjectDao;
import com.adobe.pms.entity.Project;


/*
* 
* @author
* VIkas Patidar, Saurabh Gupta
* 
*/
public class ProjectDaoImpl implements ProjectDao{
@Override
	public void addProject(Project project) throws PersistenceException, FetchException {
		Connection con=null;
		Statement stmt=null;
		int currentId = 0;
		String SQL= "SELECT max(projectId) FROM project";
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				currentId = rs.getInt(1) +1;
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get projects",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		
		
		PreparedStatement ps=null;
		SQL="INSERT INTO project (projectId,projectName,clientName, hasManager,staff, projectManagerId,projectManagerName) VALUES(?,?,?,?,?,?,?)";
		try {
			con=DBUtil.getConnection();
			ps=con.prepareStatement(SQL);
			ps.setInt(1, currentId);
			ps.setString(2, project.getProjectName());
			ps.setString(3, project.getClientName());
			ps.setBoolean(4, project.isHasManager());
			ps.setString(5,project.getStaff());
			ps.setInt(6,project.getProjectManagerId());
			ps.setString(7,project.getProjectManagerName());
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new PersistenceException("Unable to add project",e);
			
		}
		finally{
			DBUtil.releaseStatement(ps);
			DBUtil.releaseConnection(con);
		}
	}
	
@Override
	public List<Project> getAllProjects() throws FetchException {
		List<Project> projects= new ArrayList<Project>();
		Connection con=null;
		Statement stmt=null;
		//System.out.println("came inside function");
		String SQL= "SELECT projectId, projectName,hasmanager, clientName, staff, projectManagerId, projectManagerName FROM project ";
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				Project p=new Project(rs.getString("projectName"),rs.getString("clientName"));
				if(rs.getString("staff").length()!=0)p.addStaff(rs.getString("staff"));
				p.setProjectId(rs.getInt("projectId"));
					if(rs.getBoolean("hasmanager"))p.addProjectManager(rs.getInt("projectManagerId"));	
					p.setHasManager(rs.getBoolean("hasmanager"));
					p.setProjectManagerName(rs.getString("projectManagerName"));
				
				projects.add(p);
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get projects",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		//System.out.println(projects.get(0).getClientName());
		return projects;
	}
	
	@Override
	public List<Project> getAllProjectsWithoutManager() throws FetchException {
		List<Project> projects= new ArrayList<Project>();
		Connection con=null;
		Statement stmt=null;
		String SQL= "SELECT projectId, projectName, clientName, staff, empId, empName FROM project, employees where managerId = empId and hasManager=TRUE";
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				//if(rs.getInt("empId") != -1)
				{
					Project p=new Project(rs.getString("projectName"),rs.getString("clientName"));
					
					p.addStaff(rs.getString("staff"));
					p.setProjectId(rs.getInt("projectId"));
					p.addProjectManager(rs.getInt("empId"));
					projects.add(p);
				}
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get projects",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		return projects;
	}
	public Project getProject(int id) throws FetchException {
		Connection con=null;
		Statement stmt=null;
		String SQL= "SELECT projectId, hasManager,projectName, clientName, staff, projectManagerId, projectManagerName FROM project where projectId ="+ id;
		try {
			con=DBUtil.getConnection();
			stmt=con.createStatement();
			ResultSet rs= stmt.executeQuery(SQL);
			while(rs.next())
			{
				//if(rs.getInt("empId") != -1)
				{
					Project project=new Project(rs.getString("projectName"),rs.getString("clientName"));
					
					project.addStaff(rs.getString("staff"));
					project.setProjectId(rs.getInt("projectId"));
					
						project.addProjectManager(rs.getInt("projectManagerId"));
						project.setHasManager(rs.getBoolean("hasManager"));
						project.setProjectManagerName(rs.getString("projectManagerName"));
					
					return project;
				}
			}
			
		} catch (SQLException e) {
			throw new FetchException("Unable to get projects",e);
			// TODO Auto-generated catch block
		}
		finally
		{
			DBUtil.releaseStatement(stmt);
			DBUtil.releaseConnection(con);
		}
		return null;

	}


	
	public void addManager(Project project,int projectManagerId,String projectManagerName) throws PersistenceException, FetchException {
		
		Connection con=null;
		Statement stmt=null;
		PreparedStatement ps=null;
		String SQL="UPDATE project SET hasManager=true,projectManagerID="+projectManagerId+",projectManagerName='"+projectManagerName+"'WHERE projectID="+project.getProjectId();
		try {
			con=DBUtil.getConnection();
			ps=con.prepareStatement(SQL);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new PersistenceException("Unable to add project manager",e);
			
		}
		finally{
			DBUtil.releaseStatement(ps);
			DBUtil.releaseConnection(con);
		}
	}

	@Override
	public void addStaff(Project project, int empId, String empName)
			throws PersistenceException, FetchException {
		
		Connection con=null;
		Statement stmt=null;
		PreparedStatement ps=null;
		String staff=project.getStaff();
		if(!("".equals(project.getStaff()))){
			staff+='|'+Integer.toString(empId)+':'+empName;
		}
		else
			staff=Integer.toString(empId)+':'+empName;
		String SQL="UPDATE project SET staff='"+staff+"'WHERE projectID="+project.getProjectId();
		try {
			con=DBUtil.getConnection();
			ps=con.prepareStatement(SQL);
			//ps.setString(5, staff);
			ps.executeUpdate();
		} catch (SQLException e) {
			throw new PersistenceException("Unable to add employee to the project",e);
			
		}
		finally{
			DBUtil.releaseStatement(ps);
			DBUtil.releaseConnection(con);
		}
		
	}
	
	public  String get_all_project_details() throws FetchException {
		List<Project>projects=new ProjectDaoImpl().getAllProjects();
		String all_data="";
		for(Project prj2:projects){
			//System.out.println("Project: "+prj2.getProjectName());
			all_data+="Project: "+prj2.getProjectName()+"\n";
			String manName;
			if(!prj2.isHasManager())
					manName = "Not-Assigned";
			else
				manName = prj2.getProjectManagerName();
			all_data+="Project Manager: "+manName+"\n"+"Staff: \n";
			//System.out.println("Project Manager: "+manName);
			//System.out.println("Staff: \n");
			String staff= prj2.getStaff();
			 if(!("".equals(staff)))
			 {
				 
			 String[] employees=staff.split("\\|");
			 int iterator=1;
			 for(String s: employees)
			 {
				 
					 //System.out.println(iterator+++") "+s.split(":")[1]);
					 all_data+="\t"+iterator+++") "+s.split(":")[1]+"\n";
			 }
		}
			 else
				 {
//				 System.out.println("");
				 all_data+="\n";
				 }
			 all_data+="\n\n";
//			 System.out.println("\n\n");
	} 
		return all_data;
	}

}



