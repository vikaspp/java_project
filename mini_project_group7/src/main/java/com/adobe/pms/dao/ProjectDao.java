package com.adobe.pms.dao;

import java.util.List;

import com.adobe.pms.entity.Project;

/*
* 
* @author
* VIkas Patidar, Saurabh Gupta, Sahil Teotia
* 
*/
public interface ProjectDao {
	void addProject(Project project) throws PersistenceException, FetchException;
	List<Project> getAllProjects() throws FetchException;
	List<Project> getAllProjectsWithoutManager() throws FetchException;
	Project getProject(int id) throws FetchException;
	void addManager(Project project,int projectManagerId,String projectManagerName) throws PersistenceException, FetchException;
    void addStaff(Project project,int projectManagerId,String projectManagerName) throws PersistenceException, FetchException;
    public  String get_all_project_details() throws FetchException;
}
