package com.adobe.pms.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/*
* 
* @author
* VIkas Patidar, Saurabh Gupta, Sahil Teotia
* 
*/

@XmlRootElement(name="employee")
@XmlAccessorType(XmlAccessType.FIELD)
public class Employee {
	private int empId;
	private static int maxId = 100;
	private String empName;
	private String empEmail;
	private String designation;
	//private boolean isManager;
	
	public Employee(String empName, String empEmail, String designation) {
		this.empId = maxId++;
		this.empName = empName;
		this.empEmail = empEmail;
		this.designation = designation;
		//this.isManager = false;
	}
	/**
	 * 
	 */
	public Employee() {
		this.designation="";
		this.empEmail="";
		this.empName="";
		
	}
	public Employee(int empId, String empName, String empEmail, String designation) {
		this.empId = empId;
		this.empName = empName;
		this.empEmail = empEmail;
		this.designation = designation;
		//this.isManager = false;
	}
	public int getEmpId() {
		return empId;
	}
	public void setEmpId(int empId) {
		this.empId = empId;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getEmpEmail() {
		return empEmail;
	}
	public void setEmpEmail(String empEmail) {
		this.empEmail = empEmail;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	/*public boolean isManager() {
		return isManager;
	}
	public void setManager(boolean isManager) {
		this.isManager = isManager;
	}*/
	
	

}
