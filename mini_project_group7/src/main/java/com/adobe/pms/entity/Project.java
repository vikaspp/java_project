package com.adobe.pms.entity;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/*
* 
* @author
* VIkas Patidar, Saurabh Gupta, Sahil Teotia
* 
*/

@XmlRootElement(name="project")
@XmlAccessorType(XmlAccessType.FIELD)
public class Project {
	private int projectId;
	private String projectName;
	private String clientName;
	private String staff;
	private boolean hasManager;
	private int projectManagerId;
	private String projectManagerName;
	
	/**
	 * 
	 */
	public Project() {
		this.hasManager=false;
		this.projectManagerId=-1;
		this.projectManagerName="";
		this.staff="";
		this.clientName="";
		this.projectName="";
	}
	public String getProjectManagerName() {
		return projectManagerName;
	}
	public void setProjectManagerName(String projectManagerName) {
		this.projectManagerName = projectManagerName;
	}
	public int getProjectId() {
		return projectId;
	}
	public String getProjectName() {
		return projectName;
	}
	
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public int getProjectManagerId() {
		return projectManagerId;
	}
	public String getStaff() {
		return staff;
	}
	public boolean isHasManager() {
		return hasManager;
	}
	public void setHasManager(boolean hasManager) {
		this.hasManager = hasManager;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public Project(String projectName, String clientName) {
		this.projectName = projectName;
		this.clientName = clientName;
		this.hasManager = false;
		this.staff = "";
		this.projectManagerName="";
		this.projectManagerId = -1;
	}
	
	public Project(int projectId, String projectName, String clientName, String staff) {
		this.projectId = projectId;
		this.projectName = projectName;
		this.clientName = clientName;
		this.hasManager = false;
		//this.projectId = maxId++;
		this.staff =  "";
		this.projectManagerName="";
		this.projectManagerId = -1;
	}
	public void setProjectId(int projectId)
	{
		this.projectId=projectId;
	//	maxId--;
	}
	
	public boolean addProjectManager(int managerId)
	{
		if(this.hasManager == true)
		{
			return false;
		}
		this.hasManager = true;
		this.projectManagerId=managerId;
		//System.out.println("Current Project already has a Manager!");
		//System.out.println("Manager Added Successfully!");
		return true;
	
	}
	public void addStaff(String s)
	{
		if("".equals(this.staff))
		{
			this.staff = s;
		}
		else
		{
			this.staff += "|"+s;
		}
	}
	
	
}
