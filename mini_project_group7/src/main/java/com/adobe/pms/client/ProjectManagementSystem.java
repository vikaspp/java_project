package com.adobe.pms.client;

/*
 * 
 * @author
 * Vikas Patidar, 
 * Saurabh Gupta
 * 
 */

import java.io.Console;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.adobe.pms.dao.FetchException;
import com.adobe.pms.dao.PersistenceException;
import com.adobe.pms.dao.ProjectDao;
import com.adobe.pms.dao.jdbc.ProjectDaoImpl;
import com.adobe.pms.entity.Employee;
import com.adobe.pms.entity.Project;
import com.adobe.pms.dao.EmployeeDao;
import com.adobe.pms.dao.jdbc.EmployeeDaoImpl;

public class ProjectManagementSystem {
	//public boolean addEmployee(String name, String email, String designation)
	
		//add to database
	

	
	public static void main(String[] args) {
		List<Employee> emp= new ArrayList<Employee>();
//		
		Scanner reader = new Scanner(System.in);
		int input = 0;
		List<Project> projects=null;
		ProjectDao obj3= new ProjectDaoImpl();
		EmployeeDao obj= new EmployeeDaoImpl();;
		while(input!=8)
			{
			
			System.out.println("\n\nPress the desired operation:");
			System.out.println("1: Add a Project");
			System.out.println("2: Add an Employee");
			System.out.println("3: Assign Project Manager to Project");
			System.out.println("4: Add Staff to the project");
			System.out.println("5: Show all Employees");
			System.out.println("6: Show all Projects");
			System.out.println("7: To List All the projects and Employees");
			System.out.println("8: To Exit");
			input=reader.nextInt();
			reader.nextLine();
			switch(input)
			
		{
			case 1:
				System.out.println("------------Creating the Project----------");
				System.out.println("Kindly enter the following details for new porject");
				System.out.println("Project Title: ");
				String projectTitle = reader.nextLine();
				System.out.println("Client Name: ");
				String projectClient = reader.nextLine();
				System.out.println(projectTitle + " "+projectClient);
				Project p = new Project(projectTitle,projectClient);
				ProjectDao obj1 = new ProjectDaoImpl();
				try {
					//System.out.println(p.getClientName() + " "+ p.getProjectId() + " " + p.getProjectName() + " "+p.getProjectManagerId() + " "+ p.getStaff());
					
					try {
						obj1.addProject(p);
					} catch (FetchException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
					

					
				} catch (PersistenceException e1) {
					e1.printStackTrace();
				}
				break;
			case 2:
				System.out.println("---------Creating the Employee Profile---------");
				System.out.println("Kindly enter the following details for new employee");
				System.out.println("Employee Name: ");
				String empName = reader.nextLine();
				System.out.println("Employee Email: ");
				String empEmail = reader.nextLine();
				System.out.println("Employee Designation: \nPress 1 : SE\nPress 2 : Tester\n Press 3 : HelpDesk");
				int designationInput = reader.nextInt();
				String empDesignation;
				reader.nextLine();
					String[] designations= {"SE","Tester","HelpDesk"};
					empDesignation=designations[designationInput-1];
				
				Employee newEmployee = new Employee(empName,empEmail,empDesignation);
				 obj = new EmployeeDaoImpl();
				
					//System.out.println(p.getClientName() + " "+ p.getProjectId() + " " + p.getProjectName() + " "+p.getProjectManagerId() + " "+ p.getStaff());
					
					try {
						obj.addEmployee(newEmployee);
					} catch (FetchException e2) {
						// TODO Auto-generated catch block
						e2.printStackTrace();
					}
					
				 catch (PersistenceException e1) {
					e1.printStackTrace();
				}
				
				break;
			case 3:
					getProjectDetails();
					System.out.println("\n\n------- Fetching Project Managers -------");
				
				try {
					projects = obj3.getAllProjects();
				} catch (FetchException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
					
					boolean flag=false;
					for( Project prj: projects)
					{
						String manName;
						if(!prj.isHasManager())
								continue;
						else
							{
							if(!flag)
							{
								System.out.println("ManagerId\tManager-Name");
								flag=true;
							}
							manName = prj.getProjectManagerName();
							}
						System.out.println(prj.getProjectManagerId()+"\t"+manName);
					}
					if(!flag)
					System.out.println("\n Sorry!! None of the employee is a Project Manager right now\n\n");
					getEmployeeDetails();
					int projectId,empId;
					System.out.println("---------  Initializing manager assigning process  ----------");
					System.out.println("Enter project id ");
					projectId=reader.nextInt();
					try {
						Project prj= obj3.getProject(projectId);
						if(prj.isHasManager())
						{
							System.out.println("Project already has a manager");
							break;
					    }
						System.out.println("Enter employee id ");
						empId=reader.nextInt();
						reader.nextLine();
						 Employee emp1= obj.getEmployee(empId);
						 
						 
						 
							obj3.addManager(prj, empId, emp1.getEmpName());
							System.out.println("Project Manager added Successfully");
					}
						 catch (PersistenceException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					 catch (FetchException e) {
						e.printStackTrace();
					}
						
					projects=null;
				break;
			case 4:
				getProjectDetails();
				getEmployeeDetails();
				System.out.println("---------  Initializing Staff addition process  ----------");
				try {
					System.out.println("Enter project id ");
					projectId=reader.nextInt();
					Project prj= obj3.getProject(projectId);
					if(prj==null)
					{System.out.println("Not a valid project Id..Please verify");
				break;}
					System.out.println("Enter employee id ");
					empId=reader.nextInt();
					reader.nextLine();
					 Employee emp1= obj.getEmployee(empId);
					 if(emp1==null)
						{System.out.println("Not a valid employee Id..Please verify");
					break;}
					 String staff= prj.getStaff();
					 if(!("".equals(staff)))
					 {
						 
					 String[] employees=staff.split("\\|");
					 flag=false;
					 for(String s: employees)
					 {
						 if(Integer.parseInt(s.split(":")[0])==empId)
						 {
							 System.out.println("Employee is already part of this project");
								flag=true;
						 }
						 if(flag)
							 break;
					 }
					 if(flag)
					 {
						 break;
					 }
					 
					 }
					 flag=false;
					 projects= obj3.getAllProjects();
							flag=false;
						for( Project prj1: projects)
						{
							int manId;
							if(!prj1.isHasManager())
									continue;
							
								manId = prj1.getProjectManagerId();
								if(manId==empId)
								{flag=true;
								System.out.println("Employee is project manager.. Choose a different employee");
								break;
								}
								if(flag)
									break;
						}
						if(flag)
							break;
					 

						obj3.addStaff(prj, empId, emp1.getEmpName());
						System.out.println("Employee added to the project successfully");
				}
					 catch (PersistenceException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
				 catch (FetchException e) {
					e.printStackTrace();
				}
				break;
			case 5:
				getEmployeeDetails();
				break;
			case 6:
				getProjectDetails();
				break;
			case 7:
				try {
					projects=obj3.getAllProjects();
					for(Project prj2:projects){
						System.out.println("Project: "+prj2.getProjectName());
						String manName;
						if(!prj2.isHasManager())
								manName = "Not-Assigned";
						else
							manName = prj2.getProjectManagerName();
						System.out.println("Project Manager: "+manName);
						System.out.println("Staff: \n");
						String staff= prj2.getStaff();
						 if(!("".equals(staff)))
						 {
							 
						 String[] employees=staff.split("\\|");
						 int iterator=1;
						 for(String s: employees)
						 {
							 
								 System.out.println(iterator+++") "+s.split(":")[1]);
						 }
					}
						 else
							 System.out.println("");
						 System.out.println("\n\n");
				} 
				}catch (FetchException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				break;
			default:
				System.out.println("Invalid Operation Selected!!");
		   }		
		}
		System.out.println("Exited from the system");
	}





public static void getEmployeeDetails()
{
	System.out.println("\n\n------ Fetching Employees ------");
	EmployeeDao obj = new EmployeeDaoImpl();
	try {
		List<Employee> employees= obj.getAllEmployees();
		System.out.println("Emp-ID\tEmp-Name\tEmp-Email\t\tEmp-Designation");
		for( Employee emp1: employees)
		{
	
			System.out.println(emp1.getEmpId()+"\t"+emp1.getEmpName()+"\t"+emp1.getEmpEmail()+"\t\t"+emp1.getDesignation());
		}
		
	} catch (FetchException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}

}

public static void getProjectDetails()
{
	System.out.println("------- Fetching Projects -------");
	ProjectDao obj= new ProjectDaoImpl();
	try {
		List<Project> projects= obj.getAllProjects();
		System.out.println("ProjID\tProject-Name\tClient\tManager");
		for( Project prj: projects)
		{
			String manName;
			if(!prj.isHasManager())
					manName = "Not-Assigned";
			else
				manName = prj.getProjectManagerName();
			System.out.println(prj.getProjectId()+"\t"+prj.getProjectName()+"\t"+prj.getClientName()+"\t"+manName);
		}
		
	} catch (FetchException e1) {
		e1.printStackTrace();
	}

}
}
