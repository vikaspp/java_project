package com.adobe.pms.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.adobe.pms.dao.FetchException;
import com.adobe.pms.dao.PersistenceException;
import com.adobe.pms.dao.EmployeeDao;
import com.adobe.pms.dao.jdbc.EmployeeDaoImpl;
import com.adobe.pms.entity.Employee;


/*
* 
* @author
* VIkas Patidar
* 
*/
@Path("/employees")
public class EmployeeService {

	private EmployeeDao EmployeeDao = new EmployeeDaoImpl();
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<Employee> getEmployees() throws FetchException
	{
		
			
			
			
		 return EmployeeDao.getAllEmployees();
//		   //for(Employee prj:prjs)
//		   {
//			   printEmployee(prj);
//		   }
//		   return prjs;
		
	}
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response addEmployee(Employee Employee)
	{
		try {
			EmployeeDao.addEmployee(Employee);
		} catch (PersistenceException | FetchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.ok().entity("Employee Added").build();
	}
	
	@GET
	@Path("{empId}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Employee getEmployee(@PathParam("empId") int id)
	{
		try {
		Employee p1=EmployeeDao.getEmployee(id);
		if(p1==null)
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
			        .entity( "Not a valid Employee id ... Please use localhost:8080/employees to see the existing projects").type(MediaType.TEXT_PLAIN).build());		return p1;
		} catch (FetchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

public static void printEmployee(Employee prj)
{
	System.out.println(prj.getEmpId()+" "+prj.getEmpName()+" "+prj.getEmpEmail()+" "+prj.getDesignation());
}
}