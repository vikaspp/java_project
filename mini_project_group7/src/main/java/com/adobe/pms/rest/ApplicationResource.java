package com.adobe.pms.rest;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;
/*
* 
* @author
* VIkas Patidar
* 
*/
@ApplicationPath("/")
public class ApplicationResource extends ResourceConfig {

	public ApplicationResource()
	{
		packages("com.adobe.pms");
	}
}
