package com.adobe.pms.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.adobe.pms.dao.EmployeeDao;
import com.adobe.pms.dao.FetchException;
import com.adobe.pms.dao.PersistenceException;
import com.adobe.pms.dao.ProjectDao;
import com.adobe.pms.dao.jdbc.EmployeeDaoImpl;
import com.adobe.pms.dao.jdbc.ProjectDaoImpl;
import com.adobe.pms.entity.Employee;
import com.adobe.pms.entity.Project;

/*
* 
* @author
* VIkas Patidar
* 
*/

@Path("/projects")
public class ProjectService {

	private ProjectDao projectDao = new ProjectDaoImpl();
	private EmployeeDao employeeDao= new EmployeeDaoImpl();
	
	@GET
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public List<Project> getProjects() throws FetchException
	{
		
		 return projectDao.getAllProjects();
	}
	
	
	@GET
	@Path("list_all")
	@Produces({MediaType.TEXT_PLAIN})
	public String get_all() throws FetchException
	{
		
		
		String all_data=projectDao.get_all_project_details();
		
		return all_data;
	}
	
	
	
	
	
	@POST
	@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
	public Response addProject(Project Project)
	{
		try {
			projectDao.addProject(Project);
		} catch (PersistenceException | FetchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Response.ok().entity("Project Added").build();
	}
	
	@GET
	@Path("{projectId}")
	@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML,MediaType.TEXT_PLAIN})
	public Project getProject(@PathParam("projectId") int id)
	{
		try {
		Project p1=projectDao.getProject(id);
		if(p1==null)
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
	        .entity( "Not a valid Project id ... Please use localhost:8080/projects to see the existing projects").type(MediaType.TEXT_PLAIN).build());
			//System.out.println("Not a Valid id... Please use / projects to find the employees");
		return p1;
		} catch (FetchException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	@PUT
	@Path("assign_manager/{projectId}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response addManager(@PathParam("projectId") int projectId,int projectManagerId) throws FetchException, PersistenceException
	{
		Project prj= projectDao.getProject(projectId);
		if(prj==null)
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
	        .entity( "Not a valid Project id ... Please use localhost:8080/projects to see the existing projects").type(MediaType.TEXT_PLAIN).build());

		if(prj.isHasManager())
			throw new WebApplicationException(Response.status(Response.Status.CONFLICT)
			        .entity( "CONFLICT..Project already has a manager assigned to it... Please use localhost:8080/projects to see the projects with managers").type(MediaType.TEXT_PLAIN).build());
		Employee emp=employeeDao.getEmployee(projectManagerId);
		if(emp==null)
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
			        .entity( "Not a valid Employee id ... Please use localhost:8080/employees to see the existing projects").type(MediaType.TEXT_PLAIN).build());
		projectDao.addManager(prj, projectManagerId, emp.getEmpName());
		return Response.ok().entity("Manager successfully assigned to the project!").build();
	}

	@PUT
	@Path("assign_staff/{projectId}")
	@Consumes(MediaType.TEXT_PLAIN)
	public Response addStaff(@PathParam("projectId") int projectId,int empId) throws FetchException, PersistenceException
	{
		Project prj= projectDao.getProject(projectId);
		if(prj==null)
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
	        .entity( "Not a valid Project id ... Please use localhost:8080/projects to see the existing projects").type(MediaType.TEXT_PLAIN).build());
		Employee emp=employeeDao.getEmployee(empId);
		if(emp==null)
			throw new WebApplicationException(Response.status(Response.Status.NOT_FOUND)
			        .entity( "Not a valid Employee id ... Please use localhost:8080/employees to see the existing projects").type(MediaType.TEXT_PLAIN).build());
		
		String staff= prj.getStaff();
		 if(!("".equals(staff)))
		 {
			 
		 String[] employees=staff.split("\\|");
		 for(String s: employees)
		 {
			 if(Integer.parseInt(s.split(":")[0])==empId)
			 {
				 throw new WebApplicationException(Response.status(Response.Status.CONFLICT)
					        .entity( "CONFLICT..Employee is already part of this project... Please use localhost:8080/projects to see the projects staff members").type(MediaType.TEXT_PLAIN).build());
			
			 }
			 
		 }
		 
		 }
		 
		 List<Project> projects= projectDao.getAllProjects();
			for( Project prj1: projects)
			{
				int manId;
				if(!prj1.isHasManager())
						continue;
				
					manId = prj1.getProjectManagerId();
					if(manId==empId)
					{
						throw new WebApplicationException(Response.status(Response.Status.CONFLICT)
						        .entity( "CONFLICT..Employee is a project manager ... Please use localhost:8080/projects to see the projects with managers").type(MediaType.TEXT_PLAIN).build());
				
					}
					
			}
	
		projectDao.addStaff(prj, empId, emp.getEmpName());
		return Response.ok().entity("Employee is assigned to the project").build();
	}
	
	

public static void printProject(Project prj)
{
	System.out.println(prj.getProjectId()+" "+prj.getProjectName()+" "+prj.getClientName()+" "+prj.getProjectManagerId()+" "+prj.getProjectManagerName()+" "+prj.getStaff()+" "+prj.isHasManager());
}
}